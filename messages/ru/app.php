<?php

$translate = array(
    'main' => 'Основная погода',
    'description ' => 'Описание',
    'temp' => 'Температура(С)',
    'pressure ' => 'Давление(мм)',
    'humidity ' => 'Влажность(%)', 
    'wind_speed' => 'Скорость ветра (м/с)',
    'wind_deg' => 'Направление ветра',
    'clouds' => 'Облачность(%)',
    'rain' => 'Вероятность дождя',
    'snow' => 'Вероятность снега',
);

return $translate;
