<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Получаем токен через апи
     *
     * @return string|exeption
     */
    public function actionGetToken() {

        if (Yii::$app->request->post()) {
            list('login' => $login, 'password' => $password) = Yii::$app->request->post();
            if (!empty($login) && !empty($password)) {
                $ch = curl_init("http://test.n0for.ru/api/weather/auth");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('username' => $login, 'password' => $password)));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);

                $result = json_decode(curl_exec($ch));
                if (!isset($result->code) || $result->code) {
                    echo json_encode($result);
                    exit();
                }
                //@ToDO привести курл к виду
                //curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://test.n0for.ru/api/weather/auth" -d '{"username": "test", "password": "123456"}'
            }
        }
        throw new \yii\web\NotFoundHttpException("No User.");
    }


    /**
     * Получаем информацию о погоде по токену
     *
     * @return string|exeption
     */
    public function actionGetWeather() {

        if (Yii::$app->request->post()) {
            list('token' => $token, 'query' => $query) = Yii::$app->request->post();
            if (!empty($token)) {
                if (!empty($query)) {
                    $url = "http://test.n0for.ru/api/weather/weather?" . $query;
                } else {
                    $url = "http://test.n0for.ru/api/weather/weather";
                }


                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_USERPWD, "$token:");
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                $result = json_decode(curl_exec($ch));
                if (!isset($result->code) || $result->code) {
                    echo json_encode($result);
                    exit();
                }

                //@ToDO привести курл к виду
                //curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://test.n0for.ru/api/weather/auth" -d '{"username": "test", "password": "123456"}'
            }
        }
        throw new \yii\web\NotFoundHttpException("Invalid token.");
    }

}
