<?php

namespace app\controllers;

use Yii;
use app\models\City;
use yii\web\ForbiddenHttpException;
use app\models\auth\CustomAuth;
use yii\data\ActiveDataProvider;

/**
 *
 * Class ApiWeatherController - отвечает за работу Api
 * @package app\controllers
 */

class ApiWeatherController extends \yii\rest\ActiveController
{

    public $modelClass = 'app\models\City';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'auth' => ['POST'],
            'weather' => ['GET'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CustomAuth::className(),
        ];
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'weather' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider']
            ]];
    }

    /**
     * Получаем инфомрацию о токене который получается при кастомной футонтефикации
     */
    public function actionAuth()
    {
        return ['status' => 'OK', 'request' => CustomAuth::getBicycle()];
    }

    /**
     * Определяем какие поля нем необходимо вернуть
     * @return ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        return new ActiveDataProvider(array(
            'query' => City::find()->select(['name','weather']),
        ));
    }

    /**
     * Преобразуем получивщейся результат в требующийся результат (city, name}
     * @param $action - информация об произошедшем событии
     * @param array $result - массив с результатами события
     * @return array
     */
    public function afterAction($action, $result){

        $result = parent::afterAction($action, $result);
        if($action->id == 'weather'){//check controller action ID
            $new_return=array();
            foreach($result as $item){
                $new_return[]=array('name'=>$item['name'],'temp'=>$item['weather']['temp']);
            }
            $result=$new_return;
        }
        return $result;
    }


}