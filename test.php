<?php

/**
 * Тестовое задание на должность PHP-программиста
 *
 * @since ...
 * @author ...
 */
/*
 * ЗАДАНИЕ №0:
 *
 * Ознакомьтесь с описанием классов:
 *    \Common\AbstractRepository
 *    \Article\Repository
 *    \Article\Utils
 *
 * а так же со структурой базы данных (см. приложения scheme.png и scheme.sql):
 *
 * Таблица: Article
 * +------------+------------------+------+-----+---------+----------------+
 * | Field      | Type             | Null | Key | Default | Extra          |
 * +------------+------------------+------+-----+---------+----------------+
 * | article_id | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
 * | title      | varchar(255)     | NO   |     | NULL    |                |
 * | text       | text             | NO   |     | NULL    |                |
 * | created    | datetime         | NO   |     | NULL    |                |
 * | updated    | datetime         | YES  |     | NULL    |                |
 * +------------+------------------+------+-----+---------+----------------+
 *
 * Таблица: Author
 * +-----------+------------------+------+-----+---------+----------------+
 * | Field     | Type             | Null | Key | Default | Extra          |
 * +-----------+------------------+------+-----+---------+----------------+
 * | author_id | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
 * | name      | varchar(100)     | NO   |     | NULL    |                |
 * | biography | text             | NO   |     | NULL    |                |
 * +-----------+------------------+------+-----+---------+----------------+
 *
 * Таблица: AuthorArticle
 * +--------------+---------------------+------+-----+---------+-------+
 * | Field        | Type                | Null | Key | Default | Extra |
 * +--------------+---------------------+------+-----+---------+-------+
 * | article_id   | int(10) unsigned    | NO   | PRI | NULL    |       |
 * | author_id    | int(10) unsigned    | NO   | PRI | NULL    |       |
 * | contribution | tinyint(3) unsigned | NO   |     | 100     |       |
 * +--------------+---------------------+------+-----+---------+-------+
 * (поле AuthorArticle.contribution указывает на "вклад" автора в статью, 
 *  для одного автора значение устанавливается в 100, в случае соавторства
 *  сумма не превышает 100
 *
 * Таблица: Tag
 * +--------+------------------+------+-----+---------+----------------+
 * | Field  | Type             | Null | Key | Default | Extra          |
 * +--------+------------------+------+-----+---------+----------------+
 * | tag_id | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
 * | name   | varchar(45)      | NO   |     | NULL    |                |
 * +--------+------------------+------+-----+---------+----------------+
 *
 * Таблица: ArticleTag
 * +------------+------------------+------+-----+---------+-------+
 * | Field      | Type             | Null | Key | Default | Extra |
 * +------------+------------------+------+-----+---------+-------+
 * | article_id | int(10) unsigned | NO   | PRI | NULL    |       |
 * | tag_id     | int(10) unsigned | NO   | PRI | NULL    |       |
 * +------------+------------------+------+-----+---------+-------+
 */

namespace Common {

    class AbstractRepository {

        /**
         * Экземпляр класса PDO
         *
         * @var \PDO
         */
        public $database;

        public function __construct() {
            
        }

        /**
         * Получение экземпляра класса PDO
         *
         * @return \PDO
         */
        public function getDatabase() {
            $this->connect();

            return $this->database;
        }

// function getDatabase

        /**
         * Инициализация экземпляра класса PDO
         */
        public function connect() {
            if (is_null($this->database)) {
                $this->database = new \PDO('sqlite:database.sqlite');
            }
        }

// function connect

        /**
         * Запрос к БД на выгрузку набора данных
         *
         * @param string $query Запрос к базе данных
         * @param array $params Параметры запроса
         * @return array Результирующее множество
         */
        public function getMany($query, array $params = array()) {
            $statement = $this->getDatabase()->prepare($query);
            if (!empty($params)) {
                foreach ($params as $parameter => $value) {
                    $statement->bindValue($parameter, $value);
                }
            }
            try {
                $statement->execute();
            } catch (PDOException $e) {
                $arguments_str = 'Массив аргументов: ' . json_encode($argument);
                die(print_r(array($sql, debug_backtrace(), $e->getMessage() . ' ' . $arguments_str)));
            }

            return $statement->fetchAll(\PDO::FETCH_ASSOC);
        }

// function getMany

        /**
         * Запрос к БД на выгрузку записи с данными
         *
         * @param string $query Запрос к базе данных
         * @param array $params Параметры запроса
         * @return array Результирующая запись
         */
        public function getOne($query, array $params = array()) {
            $statement = $this->getDatabase()->prepare($query);
            if (!empty($params)) {
                foreach ($params as $parameter => $value) {
                    $statement->bindValue($parameter, $value);
                }
            }
            try {
                $statement->execute();
            } catch (PDOException $e) {
                $arguments_str = 'Массив аргументов: ' . json_encode($argument);
                die(print_r(array($sql, debug_backtrace(), $e->getMessage() . ' ' . $arguments_str)));
            }

            return $statement->fetch(\PDO::FETCH_ASSOC);
        }

// function getOne
    }

}

namespace Article {

    class Repository extends \Common\AbstractRepository {

        /**
         * Метод возвращает последние N-записей из таблицы Article,
         * отсортированных по дате создания (Article.created) по убыванию,
         * с данными по автору (Author) и списком указанных тегов (Tag).
         *
         * Например, при $limit = 3:
         *   [1, "Заголовок статьи №1", "Текст статьи №1", "2010-01-12", "Автор А", ["Тег 1", "Тег 2", ...]],
         *   [2, "Заголовок статьи №2", "Текст статьи №2", "2010-01-05", "Автор Б", ["Тег 2", "Тег 5", ...]],
         *   [3, "Заголовок статьи №3", "Текст статьи №3", "2010-01-01", "Автор А", []], ...
         *
         * Допущение: в качестве автора выбирается одна запись из таблицы Author с максимальным
         * значением поля Author.contribution
         *
         * @param int $limit Ограничение на вывод записей
         * @return array
         */
        public function getLatest($limit = 0) {
            $return = array();
            if (!$limit || !is_numeric($limit)) {
                $limit = '';
            } else {
                $limit = ' LIMIT 0,' . $limit;
            }
            // Автор тот кто больше написал
            $sql = 'SELECT a.id, a.title, a.text, a.created, c.name FROM Article as a LEFT JOIN AuthorArticle as b on a.article_id=b.article_id 
					LEFT JOIN Author as c on c.author_id=b.author_id 
					WHERE b.contribution=(SELECT MAX(e.contribution) FROM AuthorArticle as e WHERE e.article_id=a.article_id) 
					ORDER BY created DESC' . $limit;
            $list = $this->getMany($sql);
            if (!empty($list)) {
                $sql = 'SELECT a.name FROM Tag as a LEFT JOIN ArticleTag as b on b.tag_id=a.tag_id WHERE b.article_id=:article_id';
                foreach ($list as $item) {
                    $tags = $this->getMany($sql, array('article_id' => $item['id']));
                    $return[] = array('article_id' => $item['id'], 'article_title' => $item['title'], 'article_text' => $item['text'],
                        'article_created' => $item['created'], 'article_author' => $item['name'], 'tags_list' => $tags);
                }
            }
            return $return;
        }

// function getLatest
    }

    class Utils {

        /**
         * Метод принимает в качестве аргумента текст статьи, производит поиск ссылок и заменяет все найденные
         * ссылки на конструкции вида "текст ссылки":uri
         *
         * Например, текст вида:
         *    ... текст статьи <a class="link" href="http://www.garant.ru/company/about/" title="О компании">О компании</a> текст статьи ...
         * метод возвращает текст вида:
         *    ... текст статьи "О компании":http://www.garant.ru/company/about/ текст статьи ...
         *
         * @param string $text Текст
         * @return string
         */
        public function convertLinks($text) {
            return preg_replace('|<a[^>]*href="([\S ]+?)"[^>]*>(.*?)<\/a>|', '"$2":$1', $text);
        }

// function convertLinks
    }

}

namespace Author {

    // ...

    class Author extends \Common\AbstractRepository {
        /*
         * Получает Уникальные пары Авторов и соавторов
         * Автор1=>Автор2, тоже самое что и  Автор2=>Автор1 т.к по заданию  "неповторяющиеся пары имен авторов"
         * Еслд нужны все пары то необходимо убрать if из запроса
         * Можно было исопльзовать такую конструкцию  GREATEST(a.name, c.name),LEAST(a.name, c.name)
         * @return array
         */

        function getUniqPars() {
            $sql = 'SELECT DISTINCT if(a.name>c.name,a.name, c.name) as author1, if(a.name>c.name,c.name, a.name) as author2 ' // Получаем одинаковые пары значений
                    . 'FROM Author as a '
                    . 'LEFT JOIN AuthorArticle as b on a.author_id=b.author_id '
                    . 'JOIN Author as c LEFT JOIN AuthorArticle as d on d.author_id=c.author_id '
                    . 'WHERE c.author_id<>a.author_id  AND b.article_id=d.article_id';
            $list = $this->getMany($sql);
            $return = array();
            if (!empty($lsit)) {
                foreach ($list as $key => $value) {
                    $return[$value['author1']][] = $value['author2'];
                }
            }
            return $return;
        }

    }

}

namespace {
    /*
     * ЗАДАНИЕ №1:
     *
     * Измените / дополните описание классов
     *   \Common\AbstractRepository
     *   \Article\Repository
     *   \Article\Utils
     *
     * в соответствии с представленным ниже участком кода
     */

    use Article\Repository;
    use Article\Utils;
    use Author\Author;

$template = <<<EOT
<article>
	<h2 class="title">%d %s</h2>
	<div class="text">%s</div>
	<h4 class="author">%s, %s</h4>
	<div class="tags">%s</div>
</article>
EOT;

    $repository = new Repository();
    foreach ($repository->getLatest(10) as $article) {
        echo sprintf($template, $article['article_id'], $article['article_title'], Utils::convertLinks($article['article_text']), $article['article_created'], $article['article_author'], implode(', ', $article['tags_list']));
    }

    /*
     * ЗАДАНИЕ №2:
     *
     * Напишите метод, извлекающий из базы данных неповторяющиеся пары имен авторов (Author.name),
     * работавших в соавторстве
     */


    $author = new Author();
    $authorship = $author->getUniqPars();
    foreach ($authorship as $author => $coauthors) {
        foreach ($coauthors as $coauthor) {
            echo $author, ', ', $coauthor, "<br>\r\n";
        }
    }


    /*
     * ЗАДАНИЕ №3:
     *
     * Для массива размерности N, вида:
     * [
     *    1 => [1,  "Title 1", null, '/'],
     *    2 => [2,  "Title 2", 1,    '/about/'],
     *    3 => [3,  "Title 3", 1,    '/contacts/'],
     *    4 => [4,  "Title 4", 2,    '/about/company/'],
     *    5 => [5,  "Title 5", 2,    '/about/history/'],
     *    8 => [8,  "Title 8", null, '/eng/'],
     *    ...
     *    35 => [35, "Title 35", 8,   '/eng/about/']
     *    ...
     *    int 'id' => [int 'id', string 'title', int|null 'parent_id', string 'url']
     * ]
     *
     * напишите функцию, трансформирующую представленный массив в многомерный массив вида:
     * [
     *    1 => [
     *       2 => [
     *          4 => [],
     *          5 => []
     *       ],
     *       3 => []
     *    ],
     *    8 => [
     *       35 => []
     *    ],
     *    ...
     * ]
     *
     * где в качестве ключей используется значение поля 'id', в качестве значений - массив "дочерних" элементов
     */

    function getTree($array = array(), $parent_id = null) {
        $return = array();
        foreach ($array as $item) {
            if ($item[2] == $parent_id) {
                $return[$item[0]] = getTree($array, $item[0]);
            }
        }
        return $return;
    }

    $linear = [
        1 => [1, "Title 1", null, '/'],
        2 => [2, "Title 2", 1, '/about/'],
        3 => [3, "Title 3", 1, '/contacts/'],
        4 => [4, "Title 4", 2, '/about/company/'],
        5 => [5, "Title 5", 2, '/about/history/'],
        8 => [8, "Title 8", null, '/eng/'],
        35 => [35, "Title 35", 8, '/eng/about/']
    ];
    $nested = getTree($linear);
    var_dump($nested);

    /*
     * ЗАДАНИЕ №4:
     *
     *  Примените паттерн проектирования "одиночка" (singleton) для класса \Common\AbstractRepository
     */
    
    /* Реаализация паттерна одиночка    
      private static $instance = null;
      private function __construct() {

      }
      protected function __clone() {

      }
      static public function getInstance() {
        if(is_null(self::$_instance)) {
            self::$instance = new self();
         }
        return self::$instance;
      }

      Соответсвено все вызовы родительских классов должны идти не через конструктор,
     * а через выполнение функции getInstance
     * $repository = Repository::getInstance();

     */


    /* ЗАДАНИЕ №5:
     *
     *  Предложите оптимизацию кода и/или структуры базы данных
     */
}