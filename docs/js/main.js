$(function () {
    $('.js_get_token').on('submit', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $.post({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function ($data) {
                $resp = JSON.parse($data);
                $('.js_set_token').text('Токен:  ' + $resp.request);
                $('.js_set_token').show();
            },
            error: function () {
                $('.js_set_token').text('Произошла ошибка');
                $('.js_set_token').show();
            }
        });
    })


    $('.js_get_weather').on('submit', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $.post({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function ($data) {
                $resp = JSON.parse($data);
                $text = '';
                $.each($resp, function (index, value) {
                    $text += value.name + ' ' + value.temp + ' C <br>';
                });
                $('.js_set_weather').html($text);
                $('.js_set_weather').show();
            },
            error: function () {
                $('.js_set_weather').text('Произошла ошибка');
                $('.js_set_weather').show();
            }
        });
    })
})