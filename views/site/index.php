<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Test';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Проверка событий!</h1>

        <p class="lead">Заполните данные ниже что бы протестировать API</p>


    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Авторизация</h2>
                <form action="get-token" class="js_get_token">
                    Login:<br>
                    <input type="text" name="login" value="test">
                    <br>
                    Password:<br>
                    <input type="text" name="password" value="123456">
                    <br><br>
                    <input type="submit" value="Submit">
                </form>
                <p class="js_set_token"></p>
            </div>
            <div class="col-lg-6">
                <h2>Получение погоды</h2>
                <form action="get-weather" class="js_get_weather">
                    Token:<br>
                    <input type="text" name="token" value="">
                    <br>
                    QueryString:<br>
                    <input type="text" name="query" value="page=1">
                    <br><br>
                    <input type="submit" value="Submit">
                </form>
                <p class="js_set_weather"></p>
            </div>

        </div>

    </div>
</div>
