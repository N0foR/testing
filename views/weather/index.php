<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Погода в городах';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nameForWeather',
            'country',
            'weather' => [
                'label' => 'Погода',
                'format' => 'html',
                'value' => function ($model) {
                    return $this->render('showWeather', ['weather' => $model->weather, 'wdate' => $model->wdate]);
                },
            ],
        ],
    ]); ?>
</div>
