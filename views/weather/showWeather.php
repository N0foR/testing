<? if (!empty($weather)): ?>
    <span>Погода на: <?= $wdate ?></span>
    <table>
        <tbody>
            <? foreach ($weather as $key => $item): ?>
                <? if (!empty($item)): ?>
                    <? if (is_array($item)): ?>
                        <? foreach ($item as $currentWeater): ?>
                            <? foreach ($currentWeater as $ekey => $element): ?>
                                <tr>
                                    <td><?= Yii::t('app', $ekey) ?></td>
                                    <td>&nbsp;</td>
                                    <td><?= $element ?></td>
                                </tr>     
                            <? endforeach; ?>
                        <? endforeach; ?>

                    <? else: ?>
                        <tr>
                            <td><?= Yii::t('app', $key) ?></td>
                            <td>&nbsp;</td>
                            <td><?= $item ?></td>
                        </tr>
                    <? endif; ?>
                <? endif; ?>
            <? endforeach; ?>
        </tbody>
    </table>
<? else: ?>
    Погода еще не получена
<? endif; ?>
