<?php
namespace app\models\auth;

use Yii;
use yii\filters\auth\HttpBasicAuth;
use app\models\Users;

/**
 * Class CustomAuth авторизирует пользователя по логину и праолю переданому в теле запроса
 * через POST
 * @package app\models\auth
 */
class CustomAuth extends HttpBasicAuth
{

    /**
     * @var string - информация о токене
     */
    private static $bicycle = '';

    /**
     * {@inheritdoc}
     */
    public function authenticate($user, $request, $response)
    {
        try {
            list($username, $password) = $request->getAuthCredentials();

            if (empty($username)) {
                $request = Yii::$app->request;
                if ($request->isPost && !empty($request->post())) {
                    list('username' => $username, 'password' => $password) = $request->post();
                    if (!empty($username) && !empty($password)) {
                        $user = Users::findByUsername($username);
                        if ($user->validatePassword($password)) {
                            self::$bicycle=$user->setNewToken();
                            return $user;
                        }
                    }
                }
            } else {
                $user = Users::findIdentityByAccessToken($username);
                if ($user) {
                    return $user;
                }
            }

            return null;//['status' => '404', 'request' => ("Incorrect login '$username' or password '$password'.")];

        } catch (ExitException $e) {
            return null;//['status' => '404', 'request' => ("Incorrect login '$username' or password '$password'.")];
        }
    }

    public static function setBicycle()
    {
        return false;
    }

    /**
     * @return string возвращает информацию о токене
     */
    public static function getBicycle()
    {
        return self::$bicycle;
    }
}