<?php

namespace app\models;

use Yii;
use yii2tech\crontab\CronJob;
use yii2tech\crontab\CronTab;

/**
 * This is the model class for table "crontab".
 *
 * @property int $id
 * @property string $min Минуты
 * @property string $hour Часы
 * @property string $days Дни
 * @property string $month Месяца
 * @property string $years Года
 * @property string $command Команда
 */
class MyCrontab extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'crontab';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            //[['id', 'min', 'hour', 'days', 'month', 'years', 'command'], 'required'],
            [['id'], 'integer'],
            [['min', 'hour', 'days', 'month', 'weekDay'], 'string', 'max' => 5],
            [['command'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'min' => 'Минуты',
            'hour' => 'Часы',
            'days' => 'Дни',
            'month' => 'Месяца',
            'weekDay' => 'День недели',
            'command' => 'Команда',
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (empty($this->min)) {
                $this->min = '*';
            }
            if (empty($this->hour)) {
                $this->hour = '*';
            }
            if (empty($this->days)) {
                $this->days = '*';
            }
            if (empty($this->month)) {
                $this->month = '*';
            }
            if (empty($this->weekDay)) {
                $this->weekDay = '*';
            }

            return true;
        }
        return false;
    }
    

    private function setUpCron() {

        $list = $this->find()->all();

        $jobs = array();
        foreach ($list as $item) {
            $cronJob = new CronJob();
            $cronJob->min = $item->min;
            $cronJob->hour = $item->hour;
            $cronJob->day = $item->days;
            $cronJob->month = $item->month;
            $cronJob->weekDay  = $item->weekDay ;
            $cronJob->command = $item->command;
            $jobs[] = $cronJob;
        }
        $cronTab = new CronTab();
        $cronTab->mergeFilter=' ';//$this->merge();
        //   var_dump($jobs);
      //   exit();
        $cronTab->setJobs(
                $jobs
        );
        $cronTab->apply();
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->setUpCron();
    }

    public function afterDelete() {
        parent::afterDelete();
        $this->setUpCron();
    }

}
