<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $token Токен
 * @property string $auth_key Ключ авторизации
 * @property string $last_login Дата последнего захода
 * @property string $cdate Дата создания
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    /**
     * @var int - время жизни токена в минутах
     */
    private $tokenTime = 120;

    /**
     * @var bool - если true то пароль не меняем
     */
    private $noChangePassword = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['valid_token_time', 'cdate'], 'safe'],
            [['login', 'password', 'auth_key'], 'string', 'max' => 100],
            [['password'], 'string', 'min' => 6, 'max' => 100],
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'token' => 'Токен',
            'auth_key' => 'Ключ авторизации',
            'valid_token_time' => 'Сколько времени текущий токен валдиен',
            'cdate' => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }
            if (!empty($this->password) && !$this->noChangePassword) {
                $this->password = Yii::$app->security->generatePasswordHash($this->password);
            }
            return true;
        }
        return false;
    }

    /* ----------Авторизация-------------- */

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Функция задает новый токен для пользователя
     */
    public function setNewToken()
    {
        $this->token = md5(Yii::$app->security->generateRandomString());
        $this->valid_token_time = date('Y.m.d H:i:s', strtotime("+30 minutes", time()));
        $this->noChangePassword = true;
        $this->save();
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()->where(['=', 'token', $token])->andWhere(['>', 'valid_token_time', date('Y.m.d H:i:s')])->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

}
