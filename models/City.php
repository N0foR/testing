<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name
 * @property string $name_ru
 * @property string $country
 * @property double $lat
 * @property double $lon
 * @property string $cdate
 * @property string $sdate Дата синхронизации
 */
class City extends \yii\db\ActiveRecord {

    public $wdate;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'name', 'country', 'lat', 'lon'], 'required'],
            [['id'], 'integer'],
            [['lat', 'lon'], 'number'],
            [['cdate', 'sdate'], 'safe'],
            [['name', 'name_ru'], 'string', 'max' => 100],
            [['country'], 'string', 'max' => 3],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name_ru' => 'Название рус',
            'nameForWeather' => 'Название c проверкой',
            'country' => 'Страна',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'cdate' => 'Дата создания',
            'sdate' => 'Дата синхронизации',
            'weather' => 'Погода',
        ];
    }

    /**
     * Функция возвращает собственное поле которое формируетсья путем сравнения
     * русского и оригинального названия
     * @return string
     */
    public function getNameForWeather() {

        return $this->name_ru ?? $this->name;
    }

    /**
     *  Функция преобразует найденные данные в необходимый вид
     */
    public function afterFind() {

        if (!empty($this->weather)) {
            $this->weather = json_decode($this->weather);
            $this->wdate = date('d.m.Y H:i:s', $this->weather->dt);
            $this->weather->{'pressure '}=round($this->weather->{'pressure '}/133.322,3);
            unset($this->weather->dt);
            unset($this->weather->city_id);
        }
    }

}
