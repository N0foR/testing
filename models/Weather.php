<?php
/**
 * Created by PhpStorm.
 * User: Работа
 * Date: 06.11.2018
 * Time: 20:23
 */

namespace app\models;

use Yii;
use app\models\City;

/**
 * Модель для работы с APi погоды.
 */
class Weather
{

    /*
     * @var string - api url
     */
    private $api_url = 'http://api.openweathermap.org/data/2.5/';

    /*
     * @var string - В какой системе возвращать ответ
     */
    private $units = 'metric';

    /*
     * @var string - В какой системе возвращать ответ
     */
    private $method = 'group';

    /*
     * Функция получает данные о погоде в городах
     *
     * @param array $ids - список идентификаторов городов
     * @return array - информация о погоде
     */
    public function getWeather($ids = array())
    {
        if (empty($ids)) {
            return false;
        }
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        return $this->getResponse($this->api_url, $this->method, array('id' => implode(',', $ids), 'units' => $this->units, 'APPID' => Yii::$app->params['weather_api_key']));
    }

    /**
     * Подготавливает и выпонляет curl запрос
     * @param $url - адрерс куда необходимо сделать запрос
     * @param string $method - метод который необходимо вызвать в Апи
     * @param array $params - параметры запроса
     * @return bool|mixed - полученная инормация или false при ошибке
     */
    private function getResponse($url, $method = 'weather', $params = array())
    {
        $url = $url . $method . '?' . http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);
        if ($header['http_code'] === 200) {
            return $response;
        } else {
            return false;
        }
    }
}