<?php
namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\City;

/**
 * CitySearch represents the model behind the search form of `app\models\City`.
 */
class CitySearch extends City
{
    public $nameForWeather;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'name_ru', 'country', 'cdate', 'sdate'], 'safe'],
            [['lat', 'lon'], 'number'],
            [['nameForWeather'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'name_ru',
                'nameForWeather' => [
                    'asc' => ['name_ru' => SORT_ASC, 'name' => SORT_ASC],
                    'desc' => ['name_ru' => SORT_DESC, 'name' => SORT_DESC],
                    'label' => 'Название c проверкой',
                    'default' => SORT_ASC
                ],
                'country'
            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lat' => $this->lat,
            'lon' => $this->lon,
            'cdate' => $this->cdate,
            'sdate' => $this->sdate,
        ]);
        if (!empty($this->nameForWeather)) {
            $query->andFilterWhere(['like', 'name', $this->nameForWeather])
                ->orFilterWhere(['like', 'name_ru', $this->nameForWeather])
                ->andFilterWhere(['like', 'country', $this->country]);
        } else {
            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'name_ru', $this->name_ru])
                ->andFilterWhere(['like', 'country', $this->country]);
        }


        return $dataProvider;
    }
}

