<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Weather;
use app\models\City;

/**
 * Контроллер отвечает за команду yii get-weather-from-city
 * Поулчает информацию от Api http://api.openweathermap.org/data/2.5/
 * и записывает данные в БД
 * */
class GetWeatherFromCityController extends Controller
{
    /**
     * Запрашивает данные от сервиса погоды и заносит их в БД
     * @param int $limit - сколько городов необходимо запросить
     */
    public function actionIndex($limit = 40)
    {
        $weather = new Weather();

        $list = City::find()->orderBy(['sdate' => SORT_ASC, 'id' => SORT_ASC])->limit($limit)->all();
        $ids = array();
        foreach ($list as $item) {
            $ids[] = $item->id;
        }

        $ids_list = array_chunk($ids, 10);
        //т.к мы разбили массив на несколько равных часчтей N, то будет справедлиов утверждение что
        // $ids_list[l][m]===$ids[$l*N+m]

        foreach ($ids_list as $loop => $ids) {
            $response = $weather->getWeather($ids);
            // var_dump($ids);

            if ($response) {
                $info = json_decode($response);
                foreach ($ids as $key => $id) {
                    $list_key = $loop * 10 + $key;
                    if (!empty($info->list[$key])) {
                        $list[$list_key]->weather = json_encode($this->getWeaterFromJson($info->list[$key]));
                    }
                    //  var_dump($info->list[$key]);
                    $list[$list_key]->sdate = date('Y-m-d H:i:s');
                    $list[$list_key]->save();
                }
            }
        }
        return ExitCode::OK;
    }


    /**
     * Функция получает инофрмацию о погоде и формирует массив для записи в БД
     * @param stdClass $weater - объект с инфомрацие о погоде
     * @return array - подготовленный массив с инфомрацией о погоде
     */
    private function getWeaterFromJson($weater)
    {
        $weater_cond = array();
        foreach ($weater->weather as $element) {
            $weater_cond[] = array('main' => $element->main, 'description ' => $element->description,);
        }
        return array(
            'weather' => $weater_cond,
            'temp' => $weater->main->temp ?? false,
            'pressure ' => $weater->main->pressure ?? false,
            'humidity ' => $weater->main->humidity ?? false,
            'wind_speed' => $weater->wind->speed ?? false,
            'wind_deg' => $weater->wind->deg ?? false,
            'clouds' => $weater->clouds->all ?? false,
            'rain' => $weater->rain{'3h'} ?? false,
            'snow' => $weater->snow->{'3h'} ?? false,
            'city_id' => $weater->id  ?? false,
            'dt' => $weater->dt ?? false,
        );
    }


}

