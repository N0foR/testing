<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\City;

/**
 * Контроллер отвечает за команду yii add-city-to-base
 * Позвоялет загружать информацию о городах из json файла
 */
class AddCityToBaseController extends Controller
{
    /**
     * Загружает информацию о городах из json
     */
    public function actionIndex()
    {
        if (file_exists(__DIR__ . '/data/city.list.json')) {

            $list = json_decode(file_get_contents(__DIR__ . '/data/city.list.json'));
            //  print_r($list);
            $count = array();
            foreach ($list as $item) {
                if ($model = City::findOne($item->id) === null) {
                    $model = new City();
                    $model->id = $item->id ;
                    $model->name = $item->name;
                    $model->country = $item->country;
                    $model->lon = $item->coord->lon;
                    $model->lat = $item->coord->lat;
                    $model->isNewRecord = true;
                    $count[] = $model->save();
                }
            }
            echo "Insert: " . count($count) . "\n";
        }
        return ExitCode::OK;
    }
}
