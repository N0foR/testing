<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use yii2tech\crontab\CronJob;
use yii2tech\crontab\CronTab;
use app\models\MyCrontab;

/**
 * Контроллер отвечает за команду yii add-crontab
 * Изменяет планировщик задач с учетеом заданной информации из БД
 * */
class AddCrontabController extends Controller
{
    /**
     * Задает новое расписание из БД
     */
    public function actionIndex()
    {
        $model=new MyCrontab();
        $list = $model->find()->all();
        $jobs = array();
        foreach ($list as $item) {
            $cronJob = new CronJob();
            $cronJob->min = $item->min;
            $cronJob->hour = $item->hour;
            $cronJob->day = $item->days;
            $cronJob->month = $item->month;
          //  $cronJob->year = $item->years;
            $cronJob->command = $item->command;
            $jobs[] = $cronJob;
        }
        $cronTab = new CronTab();
        var_dump($list,$jobs);
        //    exit();
        $cronTab->setJobs(
                $jobs
        );
        $cronTab->apply();

        return ExitCode::OK;
    }
}
